module.exports =
  getMenus: (menuLevel) ->
    if not menuLevel
      menuLevel = 'Top menu'
    filter =
      type: 'menu'
      parent: menuLevel
    @getCollection('documents').findAllLive(filter).sortArray(name: 1)

  getCurrentMenu: (documentUrl) ->
    if not documentUrl
      return null
    filter =
      type: 'menu'
      href: documentUrl
    menu = @getCollection('documents').findOne(filter)
    if not menu
      return null
    return menu.toJSON()

  getTopSubMenu: (documentUrl) ->
    if not documentUrl
      return null
    menu = @getCollection('documents').findOne({type: 'menu', href: documentUrl})
    loop
      break if not menu or menu.meta.attributes.parent is 'Top menu'
      menu = @getCollection('documents').findOne({type: 'menu', title: menu.meta.attributes.parent})

    if not menu
      return null
    return menu.toJSON()

  getBreadCrumbs: (documentUrl) ->
    if not documentUrl
      return null

    result =[{l:'/',t:'Home'}]
    menu = @getCollection('documents').findOne({type: 'menu', href: documentUrl})

    if menu
      loop
        result.splice(1, 0, {l:menu.meta.attributes.href,t:menu.meta.attributes.title})
        break if not menu or menu.meta.attributes.parent is 'Top menu'
        menu = @getCollection('documents').findOne({type: 'menu', title: menu.meta.attributes.parent})

    return result

  getPages: ->
    filter =
      type: 'page'
      visibility: 'Visible'
    @getCollection('documents').findAllLive(filter).sortArray(name: 1)

  getNews: (newsStatus) ->
    if not newsStatus
      newsStatus = 'Active'
    filter =
      type: 'news'
      visibility: 'Visible'
      status: newsStatus
    @getCollection('documents').findAllLive(filter).sortArray(name: 1)

  getBlocks: (blockType) ->
    if not blockType
      blockType = 'Home'
    filter =
      type: 'block'
      visibility: 'Visible'
      blockType: blockType
    @getCollection('documents').findAllLive(filter).sortArray(name: 0)

  getForm: require('./templateFn/getForm')

  renderForm: require('./templateFn/renderForm')

  getParams: require('./templateFn/getParams')

  getPageThumbnail: require('./templateFn/getPageThumbnail')
