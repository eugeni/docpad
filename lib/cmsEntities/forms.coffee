module.exports =
  name: ['Form', 'Forms']
  list:
    fields: [
      name: 'Name'
      value: ->
        @name
    ]
    data: ->
      filter =
        type: 'forms'
      return @docpad.getCollection('documents').findAll(filter)
  form:
    url: ->
      "/forms/#{@slugify @name}"
    ext: 'html'
    meta:
      name: ->
        @name
      email: ->
        @email
      formJson: ->
        @formJson
      pageType: ->
        @pageType
      visibility: ->
        @visibility
      type: 'forms'
      layout: 'form'
      write: false
    components: [
      field: 'name'
      type: 'text'
    ,
      field: 'email'
      label: 'Email'
      type: 'text'
    ,
      field: 'pageType'
      label: 'Page type where form should be showed'
      type: 'my-choice'
      data: ->
        return [
          value: 'page'
          title: 'Pages'
        ,
          value: 'news'
          title: 'News'
        ]
    ,
      field: 'formTitle'
      label: 'Title'
      type: 'text'
    ,
      field: 'formDescription'
      label: 'Description'
      optional: true
      type: 'text'
    ,
      field: 'formSubtitle'
      label: 'Subtitle'
      optional: true
      type: 'text'
    ,
      field: 'formButton'
      label: 'Submit button name'
      type: 'text'
    ,
      field: 'visibility'
      label: 'Visibility'
      type: 'choice'
      expanded: true
      data: ->
        ['Visible', 'Not visible']
    ,
      field: 'formJson'
      label: 'Form Elements'
      type: 'formbuilder'
    ]
