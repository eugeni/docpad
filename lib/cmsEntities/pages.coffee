module.exports =
  name: ['Page', 'Pages']
  list:
    fields: [
      name: 'Name'
      value: ->
        @title
    ,
      name: 'Path'
      value: ->
        @cms.pagePath
    ,
      name: 'URL'
      html: ->
        "<a href=\"#{@url}\">#{@url}</a>"
    ]
  #          filters: [
  #            name:   'Path'
  #            data:   ->
  #              types = []
  #              filter = type: 'pageType'
  #              for item in @docpad.getCollection('documents').findAll(filter).sortArray(date: 0)
  #                correctPath = "/#{item.pagePath}/"
  #                correctPath = correctPath.replace /\/\//, '/'
  #                correctPath = correctPath.replace /\/\//, '/' # if pagePath == '/' then first value of correctPath contain '///'
  #                types.push correctPath
  #
  #              console.log types
  #              return types
  #          ]
    data: ->
      filter =
        type: 'page'
      #            if @path?
      #              correctPath = "/#{@path}/"
      #              correctPath = correctPath.replace /\/\//, '/'
      #              correctPath = correctPath.replace /\/\//, '/' # if pagePath == '/' then first value of correctPath contain '///'
      #              filter.pagePath = correctPath
      #            console.log filter
      return @docpad.getCollection('documents').findAll(filter)

  form:
    url: ->
      correctPath = "/#{@pagePath}/"
      correctPath = correctPath.replace /\/\//, '/'
      correctPath = correctPath.replace /\/\//, '/' # if pagePath == '/' then first value of correctPath contain '///'
      return "#{correctPath}#{@slugify @title}"
    ext: 'html'
    meta:
      title: ->
        @title
      type: 'page'
      layout: 'page'
      visibility: ->
        @visibility
      write: ->
        return (@visibility == 'Visible')
      render: ->
        return (@visibility == 'Visible')
      pagePath: ->
        @pagePath
      form: ->
        @form
    content: ->
      @content
    components: [
      field: 'title'
      type: 'text'
    ,
      field: 'content'
      label: 'Content'
      type: 'tiny-mce'
      height: 450
    ,
      field: 'visibility'
      label: 'Visibility'
      type: 'choice'
      expanded: true
      data: ->
        ['Visible', 'Not visible']
    ,
      field: 'pagePath'
      label: 'Page path'
      type: 'text'
    ,
      field: 'form'
      label: 'Form for this page'
      type: 'choice'
      optional: true
      data: ->
        formList = ['']
        filter =
          type: 'forms'
        for item in @docpad.getCollection('documents').findAll(filter).sortArray(name: 1)
          formList.push item.name
        return formList
    ,
      field: 'image'
      type: 'file'
      use: 'standard'
      optional: true
      images:
        standard:
          url: ->
            "/images/pages/#{@slugify @title}.#{@ext}"
          width: 266
          height: 99999999999
    ]
