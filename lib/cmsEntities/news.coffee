module.exports =
  name: ['News', 'News']
  list:
    fields: [
      name: 'Name'
      value: ->
        @title
    ,
      name: 'Date'
      html: ->
        @date
    ,
      name: 'URL'
      html: ->
        "<a href=\"#{@url}\">#{@url}</a>"
    ]
    data: ->
      filter =
        type: 'news'
      return @docpad.getCollection('documents').findAll(filter).sortArray(date: 1)

  form:
    url: ->
      "/news/#{@slugify @title}"
    ext: 'html'
    meta:
      title: ->
        @title
      type: 'news'
      layout: 'page'
      visibility: ->
        @visibility
      write: ->
        return (@visibility == 'Visible')
      render: ->
        return (@visibility == 'Visible')
      date: ->
        new Date(@date)
      short: ->
        @short
      status: ->
        @status
    content: ->
      @content
    components: [
      field: 'title'
      type: 'text'
    ,
      field: 'short'
      type: 'textarea'
      height: 50
    ,
      field: 'content'
      label: 'Content'
      type: 'tiny-mce'
      height: 450
    ,
      field: 'visibility'
      label: 'Visibility'
      type: 'choice'
      expanded: true
      data: ->
        ['Visible', 'Not visible']
    ,
      field: 'status'
      label: 'Status'
      type: 'choice'
      expanded: true
      data: ->
        ['Active', 'Archive']
    ,
      field: 'date'
      type: 'date'
      time: false
    ,
      field: 'image'
      type: 'file'
      use: 'standard'
      optional: true
      images:
        standard:
          url: ->
            "/images/news/#{@slugify @title}.#{@ext}"
          width: 266
          height: 99999999999
    ]
