module.exports =
  name: ['Image', 'Images']
  list:
    fields: [
      name: 'URL'
      html: ->
        "<a href=\"#{@cms.image.standard.url}\">#{@cms.image.standard.url}</a>"
    ,
      name: 'Image'
      html: ->
        return '<div style="height:32px"><img src="'+@cms.image.square.url+'" style="width:32px;height:32px" alt="image" /></div>'
    ]
    data: ->
      filter =
        type: 'image'
      return @docpad.getCollection('documents').findAll(filter)

  form:
    url: ->
      "/images/#{@slugify @name}"
    ext: 'html'
    meta:
      name: ->
        "#{@slugify @name}.#{@ext}"
      type: 'image'
      image: @image
      write: false
      render: false
    components: [
      field: 'name'
      type: 'text'
    ,
      field: 'image'
      type: 'file'
      use: 'square'
      optional: true
      images:
        square:
          url: ->
            "/images/uploaded/#{@slugify @name}.sq.#{@ext}"
          width: 32
          height: 32
          crop: true
        standard:
          url: ->
            "/images/uploaded/#{@slugify @name}.#{@ext}"
          width: 9999999
          height: 9999999
    ]
