module.exports =
  name: ['Params', 'Params']
  unique: true
  form:
    url: ->
      "/params"
    ext: 'html'
    meta:
      type: 'params'
      write: false
      render: false
    components: [
      field: 'pageImage'
      label: 'Thumbnail for Pages'
      type: 'file'
      use: 'standard'
      optional: true
      images:
        standard:
          url: ->
            "/images/pages-thumbnail.#{@ext}"
          width: 266
          height: 99999999999
    ,
      field: 'newsImage'
      label: 'Thumbnail for News'
      type: 'file'
      use: 'standard'
      optional: true
      images:
        standard:
          url: ->
            "/images/news-thumbnail.#{@ext}"
          width: 266
          height: 99999999999
    ]
