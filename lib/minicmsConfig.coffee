module.exports =
  secret: 'tshortk,ou6 mosi6stsrjsyjtyhdtyhdrt sjdytdjy'
  auth: (login, password, callback) ->
    if login is 'admin' and password is 'password'
      callback null, true
    else
      callback "Invalid login or password.", false

  models: [

    name: ['Menu', 'Menus']
    list:
      fields: [
        name: 'Name'
        value: ->
          @title
      ,
        name: 'Parent'
        value: ->
          @parent
      ,
        name: 'URL'
        value: ->
          @href
      ]
      data: ->
        filter =
          type: 'menu'
        return @docpad.getCollection('documents').findAll(filter)
    form:
      url: ->
        if @parent != "Top menu"
          "/menu/#{@slugify @parent}/#{@slugify @title}"
        else
          "/menu/#{@slugify @title}"
      ext: 'html.md'
      meta:
        title: ->
          @title
        type: 'menu'
        href: ->
          if @page is '_'
            return @href
          else
            return @page
        parent: ->
          @parent
        write: false
        render: false
      content: ->
        @title
      components: [
        field: 'title'
        label: 'Title'
        type: 'text'
      ,
        field: 'page'
        label: 'Page'
        type: 'my-choice'
        data: ->
          pageList = [
            value: '_'
            title: 'No'
          ]
          pageFilter =
            type: 'page'
          for item in  @docpad.getCollection('documents').findAll(pageFilter).sortArray(date: 0)
            pageList.push value: item.url, title: item.title
          return pageList
      ,
        field: 'href'
        label: 'URL'
        type: 'text'
        optional: true
        value: ->
          if @page is '_'
            return @href
          else
            return @page
      ,
        field: 'parent'
        label: 'Parent'
        type: 'choice'
        data: ->
          menuList = [
#                value: '_'
#                title: 'Top menu'
            'Top menu'
          ]
          filterMenu =
            type: 'menu'
          for item in @docpad.getCollection('documents').findAllLive(filterMenu).sortArray(date: -1)
#                menuList.push value: item.cms.id, title: item.title
            menuList.push item.title
          return menuList
      ]
  ,
    require('./cmsEntities/pagesParams')
  ,
    require('./cmsEntities/pages')
  ,
    require('./cmsEntities/news')
  ,

    name: ['Block', 'Blocks']
    list:
      fields: [
        name: 'Name'
        value: ->
          @name
      ,
        name: 'Type'
        value: ->
          @blockType
      ]
      data: ->
        filter =
          type: 'block'
        return @docpad.getCollection('documents').findAll(filter)

    form:
      url: ->
        "/block/#{@slugify @name}"
      ext: 'html.eco'
      meta:
        name: ->
          @name
        type: 'block'
        blockType: ->
          @blockType
        visibility: ->
          @visibility
        write: false
        render: false
      content: ->
        @content
      components: [
        field: 'name'
        type: 'text'
      ,
        field: 'content'
        label: 'Content'
        type: 'tiny-mce'
      ,
        field: 'visibility'
        label: 'Visibility'
        type: 'choice'
        expanded: true
        data: ->
          ['Visible', 'Not visible']
      ,
        field: 'blockType'
        label: 'Block type'
        type: 'choice'
        data: ->
          ['Home']
      ]
  ,
    require('./cmsEntities/images')
  ,
    require('./cmsEntities/forms')
  ]
