module.exports = function(formName) {
    if (!formName) {
        return null;
    }

    var filter = {
        type: 'forms',
        name: formName,
        visibility: 'Visible'
    };

    return docpad.getCollection('documents').findOne(filter); // Backbone.Model object will be returned if document exists
};
