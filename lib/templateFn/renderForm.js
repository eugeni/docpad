var getForm = require('./getForm');

module.exports = function(document) {
    var result = '';
    var formModel
    if (document.form) {
        formModel = getForm(document.form);
    }

    if (!formModel && document.type) {
        var filter = {
            type: 'forms',
            pageType: document.type,
            visibility: 'Visible'
        };
        formModel = docpad.getCollection('documents').findOne(filter)
    }

    if (formModel) {
        result = formModel.getOutContent() || '';
    }

    return result;
};
