var getParams = require('./getParams');

module.exports = function(document) {
    if (document.cms && document.cms.image) {
        return document.cms.image.standard.url;
    }

    var params = getParams();
    if (params && params.cms[document.type + 'Image']) {
        return params.cms[document.type + 'Image'].standard.url;
    }

    return "/images/default_sub1.jpg";
};
