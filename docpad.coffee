module.exports =
  port: 9778

  templateData:
    require('./lib/templateData')

  plugins:
    minicms:
      require('./lib/minicmsConfig')

    contactify:
      require('./lib/contactifyConfig')
